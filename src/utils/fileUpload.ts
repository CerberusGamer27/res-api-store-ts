import multer from "multer";
import path from "path";

const storage = (destination: string)=> multer.diskStorage({
    destination: destination,
    filename: (file: any, cb: any) => {
        return cb(null, `${file.fieldname}_${Date.now()}${path.extname(file.originalname)}`)
    }
})


const fileUpload = (destination: string) =>multer({
    storage: storage(destination),
    limits: {
        fileSize:  3 * 1024 * 1024, //Max size of file 3MB,
    },
    fileFilter: (file: any, cb: any) => {
        if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg") {
          cb(null, true);
        } else {
          cb(null, false);
          return cb(new Error('Solo formatos .png, .jpg and .jpeg permitidos'));
        }
      }
}).single('productImage')

export default fileUpload