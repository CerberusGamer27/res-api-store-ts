import mongoose, { model, Schema } from "mongoose";
import { IProduct } from "./IProduct";
import { BACKEND_URL } from "../config/config";

const DataSchema = new Schema<IProduct>({
    brand: {
        type: String,
        required: [true, 'Ingrese la marca del producto'],
        trim: true,
        maxLength: [50, 'La marca del producto no puede tener mas de 100 caracteres']
    },
    model: {
        type: String,
        required: [true, 'Ingrese el modelo del producto'],
        trim: true,
        maxLength: [50, 'El modelo del producto no puede tener mas de 50 caracteres']
    },
    presentation: {
        type: String,
        required: false,
        trim: true,
        maxLength: [10, 'La presentacion del producto no puede tener mas de 10 caracteres']
    },
    purchasePrice: {
        type: Number,
        required: [true, 'Ingrese el precio de compra del producto']
    },
    salePrice: {
        type: Number,
        required: [true, 'Ingrese el precio de venta del producto']
    },
    saleGain: {
        type: Number,
        required: [true, 'Ingrese el precio de venta del producto']
    },
    status: {
        type: Boolean,
        required: [true, 'Ingrese el estado del producto']
    },
    productImage: {
        type: String,
        required: false
    }
}, { timestamps: true, versionKey: false });

// Middleware to set the URL of picture

DataSchema.post('find', (docs) => {
    if (Array.isArray(docs)) {
        for (let index = 0; index < docs.length; index++) {
            const doc = docs[index]
            let getImageName = doc.productImage.split('images')[1];
            if (getImageName !== undefined) {
                doc.productImage = `https://${BACKEND_URL}/user${getImageName}`;
            }
        }
    }
    return docs
});

DataSchema.post('findOne', (document) => {

    const doc = document
    let getImageName = doc.productImage.split('images')[1];
    // doc.productImage = `http://localhost:8000/user${getImageName}`
    if (getImageName !== undefined) {
        doc.productImage = `https://${BACKEND_URL}/user${getImageName}`;
    }

    return doc
});

const Product = mongoose.model<IProduct>('Product', DataSchema, 'products');

export default Product;