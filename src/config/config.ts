import dotenv from 'dotenv';

dotenv.config();

const APP_PORT = process.env.APP_PORT
const DB_URL: string | undefined = process.env.DB_URL
const BACKEND_URL: string | undefined = process.env.BACKEND_URL;

export {
    APP_PORT,
    DB_URL,
    BACKEND_URL
}