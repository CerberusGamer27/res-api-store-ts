import express, { Express, RequestHandler } from "express";
import Product from "../models/Product";
import { createProduct, getAllProducts, getProductDetails, updateProduct, deleteProduct } from "../controllers/ProductController";
import fileUpload from "../utils/fileUpload";

const router = express.Router();

router.post('/product', fileUpload("./storage/images"), createProduct);
router.get('/products', getAllProducts);
router.get('/product/:id', getProductDetails)
router.patch('/product/:id', fileUpload("./storage/images"), updateProduct);
router.delete('/product/:id', deleteProduct)

export default router

