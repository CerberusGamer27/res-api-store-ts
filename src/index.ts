import express, { Express, NextFunction, Request, Response } from 'express';
import { APP_PORT, DB_URL } from './config/config';
import mongoose, { Connection, ConnectOptions, mongo } from 'mongoose';
import router from './routes/productRoutes';
import bodyParser from 'body-parser';
import multer from 'multer';


const app: Express = express();

mongoose.set('strictQuery', true);
mongoose.connect(`${DB_URL}`, <ConnectOptions>{
  useNewUrlParser: true,
  useUnifiedTopology: true,
  
})

const db: Connection = mongoose.connection;

db.on('error', console.error.bind(console, 'Connection Error'));
db.once('open', () => {
  console.log(`DB Connected to ${db.name}`);
})

app.use(bodyParser.json());

// Static URL of Image Defined
app.use('/user', express.static('storage/images'))
app.use("/api", router);

app.use((err: any, req: Request, res: Response, next: NextFunction) => {
  if (err instanceof multer.MulterError) { // Multer-specific errors
      return res.status(418).json({
          err_code: err.code,
          err_message: err.message,
      });
  } else { // Handling errors for any other cases from whole application
      return res.status(500).json({
          err_code: 409,
          err_message: "Something went wrong!"
      });
  }
});

//Undefined Route Implement
app.use('*', (req: Request, res: Response)=>{
  res.status(404).json({code:404, status:"fail", data:"Not Found"})
});

app.listen(APP_PORT, () => {
  console.log(`⚡[server]: Server is running at http://localhost:${APP_PORT}`);
});
